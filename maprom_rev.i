VERSION		EQU	1
REVISION	EQU	6
DATE	MACRO
		dc.b	'18.11.2021'
	ENDM
VERS	MACRO
		dc.b	'maprom 1.6'
	ENDM
VSTRING	MACRO
		dc.b	'maprom 1.6 (18.11.2021)',13,10,0
	ENDM
VERSTAG	MACRO
		dc.b	0,'$VER: maprom 1.6 (18.11.2021)',0
	ENDM
